import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserDTO } from './dto/user.dto';
import { User } from './entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import { LoginResDTO } from './dto/login-response.dto';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async register(user: UserDTO) {
    const userFound = await this.userRepository.findOneBy({
      name: user.name,
    });

    if (userFound) {
      return new HttpException('El usuario ya existe', HttpStatus.CONFLICT);
    }

    const { password } = user;
    const plainToHash = password; //await hash(password, 10);
    user = { ...user, password: plainToHash };
    const newUser = this.userRepository.create(user);
    return this.userRepository.save(newUser);
  }

  async login(user: UserDTO): Promise<LoginResDTO | HttpException> {
    const { name, password } = user;
    const userFound = await this.userRepository.findOneBy({
      name: name,
    });

    if (!userFound) {
      return new HttpException('El usuario no existe', HttpStatus.NOT_FOUND);
    }

    const checkPassword = password === userFound.password; //await compare(password, userFound.password);

    if (!checkPassword) {
      return new HttpException('Error al iniciar sesión', HttpStatus.FORBIDDEN);
    }

    const payload = { id: userFound.id, name };

    const data = {
      user: userFound,
      token: this.jwtService.sign(payload),
    };

    return data;
  }
}
