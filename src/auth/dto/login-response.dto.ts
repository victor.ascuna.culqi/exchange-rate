import { ApiProperty } from '@nestjs/swagger';
import { User } from '../entities/user.entity';

export class LoginResDTO {
  @ApiProperty()
  user: User;

  @ApiProperty({
    default: 'token',
  })
  token: string;
}
