import { ApiProperty } from '@nestjs/swagger';

export class UserDTO {
  @ApiProperty({
    default: 'admin',
  })
  name: string;

  @ApiProperty({
    default: 'admin',
  })
  password: string;
}
