import { Controller, Post, Body, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserDTO } from './dto/user.dto';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { LoginResDTO } from './dto/login-response.dto';
import { JwtAuthGuard } from './jwt-auth.guard';
import { User } from './entities/user.entity';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly usersService: AuthService) {}

  @Post('register')
  @ApiResponse({ status: 201, description: 'Created.', type: User })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({ status: 409, description: 'El usuario ya existe.' })
  @UseGuards(JwtAuthGuard)
  register(@Body() user: UserDTO) {
    return this.usersService.register(user);
  }

  @Post('login')
  @ApiResponse({ status: 201, description: 'Login.', type: LoginResDTO })
  @ApiResponse({ status: 403, description: 'El usuario no existe.' })
  @ApiResponse({ status: 404, description: 'Error al iniciar sesión.' })
  login(@Body() user: UserDTO) {
    return this.usersService.login(user);
  }
}
