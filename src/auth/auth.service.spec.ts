import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { User } from './entities/user.entity';

describe('AuthService', () => {
  let service: AuthService;
  let mockRepository;
  let mockJwtService;

  beforeEach(async () => {
    mockRepository = {
      findOneBy: jest.fn(),
      create: jest.fn(),
      save: jest.fn(),
    };

    mockJwtService = {
      sign: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: getRepositoryToken(User),
          useValue: mockRepository,
        },
        {
          provide: JwtService,
          useValue: mockJwtService,
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('service-login', () => {
    it('should return User and Token', async () => {
      const mockToken = 'Token';
      const mockUser = { id: 1, name: 'admin', password: 'admin' };
      await mockJwtService.sign.mockReturnValueOnce(mockToken);
      await mockRepository.findOneBy.mockResolvedValue(mockUser);

      const user = { name: 'admin', password: 'admin' };
      const result = await service.login(user);
      expect(result).toStrictEqual({
        token: 'Token',
        user: { id: 1, name: 'admin', password: 'admin' },
      });
    });
  });

  describe('service-register', () => {
    it('should return User', async () => {
      const mockNewUser = { id: 1, name: 'user', password: 'user' };
      await mockRepository.create.mockResolvedValue(mockNewUser);
      await mockRepository.save.mockResolvedValue(mockNewUser);

      const user = { name: 'user', password: 'user' };
      const result = await service.register(user);
      expect(result).toStrictEqual({ id: 1, name: 'user', password: 'user' });
    });
  });
});
