import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { JwtService } from '@nestjs/jwt';

describe('AuthController', () => {
  let controller: AuthController;
  let mockRepository;
  let mockJwtService;

  beforeEach(async () => {
    mockRepository = {
      findOneBy: jest.fn(),
      create: jest.fn(),
      save: jest.fn(),
    };

    mockJwtService = {
      sign: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        AuthService,
        {
          provide: getRepositoryToken(User),
          useValue: mockRepository,
        },
        {
          provide: JwtService,
          useValue: mockJwtService,
        },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('login', () => {
    it('should return User and Token', async () => {
      const mockToken = 'Token';
      const mockUser = { id: 1, name: 'admin', password: 'admin' };
      await mockJwtService.sign.mockReturnValueOnce(mockToken);
      await mockRepository.findOneBy.mockResolvedValue(mockUser);

      const user = { name: 'admin', password: 'admin' };
      const result = await controller.login(user);
      expect(result).toStrictEqual({
        token: 'Token',
        user: { id: 1, name: 'admin', password: 'admin' },
      });
    });
  });

  describe('register', () => {
    it('should return User', async () => {
      const mockUser = { id: 1, name: 'user', password: 'user' };
      await mockRepository.create.mockResolvedValue(mockUser);
      await mockRepository.save.mockResolvedValue(mockUser);

      const user = { name: 'user', password: 'user' };
      const result = await controller.register(user);
      expect(result).toStrictEqual({ id: 1, name: 'user', password: 'user' });
    });
  });
});
