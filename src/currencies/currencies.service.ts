import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateCurrencyDTO } from './dto/create-currency.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Currency } from './entities/currency.entity';
import { HttpService } from '@nestjs/axios';
import { GetExchangeDTO } from './dto/get-exchange.dto';
import { ResponseExchangeDTO } from './dto/response-exchange.dto';

@Injectable()
export class CurrenciesService {
  constructor(
    @InjectRepository(Currency)
    private readonly currenciesRepository: Repository<Currency>,
    private readonly httpService: HttpService,
  ) {}

  async create(currency: CreateCurrencyDTO) {
    const name = currency.name.trim().toLocaleUpperCase();

    const currFound = await this.currenciesRepository.findOneBy({
      name,
    });

    if (currFound) {
      return new HttpException(
        'Ya se añadio el tipo de moneda',
        HttpStatus.CONFLICT,
      );
    }

    const data = await this.httpService.axiosRef.get(
      process.env.EXCHANGE_RATE_API,
    );

    const value = data.data.rates[name];

    if (!value) {
      return new HttpException(
        'El tipo de moneda no existe',
        HttpStatus.BAD_REQUEST,
      );
    }

    const newCurrency = this.currenciesRepository.create({
      name,
      value,
    });
    return this.currenciesRepository.save(newCurrency);
  }

  async findAll() {
    const currencies = await this.currenciesRepository.find();
    return currencies.map(function (curr) {
      return curr.name;
    });
  }

  async getExchange(
    exchange: GetExchangeDTO,
  ): Promise<ResponseExchangeDTO | HttpException> {
    const currency_ori = await this.currenciesRepository.findOneBy({
      name: exchange.currency_ori.trim().toLocaleUpperCase(),
    });
    const currency_des = await this.currenciesRepository.findOneBy({
      name: exchange.currency_des.trim().toLocaleUpperCase(),
    });

    if (!currency_ori) {
      return new HttpException(
        'No se encontro el tipo de moneda origen',
        HttpStatus.NOT_FOUND,
      );
    }

    if (!currency_des) {
      return new HttpException(
        'No se encontro el tipo de moneda destino',
        HttpStatus.NOT_FOUND,
      );
    }

    const exchange_rate = currency_des.value / currency_ori.value;

    return {
      ...exchange,
      amount_exchanged: exchange.amount * exchange_rate,
      exchange_rate,
    };
  }

  async update() {
    const currencies = await this.currenciesRepository.find();

    const updates = currencies.map(function (curr) {
      return curr.updated_at.getTime();
    });

    const min_update = new Date(Math.min(...updates));
    const now = new Date();

    if (
      !(
        min_update.getUTCFullYear() === now.getUTCFullYear() &&
        min_update.getUTCMonth() === now.getUTCMonth() &&
        min_update.getUTCDate() === now.getUTCDate()
      )
    ) {
      const data = await this.httpService.axiosRef.get(
        process.env.EXCHANGE_RATE_API,
      );

      for (const curr of currencies) {
        this.currenciesRepository.update(
          { id: curr.id },
          { value: data.data.rates[curr.name] },
        );
      }
      return true;
    }

    return false;
  }
}
