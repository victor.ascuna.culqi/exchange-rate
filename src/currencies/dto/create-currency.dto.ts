import { ApiProperty } from "@nestjs/swagger";

export class CreateCurrencyDTO {
  @ApiProperty({
    default: 'pen',
  })
  name: string;
}
