import { ApiProperty } from "@nestjs/swagger";

export class ResponseExchangeDTO {
  @ApiProperty()
  amount: number;

  @ApiProperty()
  amount_exchanged: number;

  @ApiProperty()
  currency_ori: string;

  @ApiProperty()
  currency_des: string;

  @ApiProperty()
  exchange_rate: number;
}
