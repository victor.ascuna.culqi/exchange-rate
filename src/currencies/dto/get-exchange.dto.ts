import { ApiProperty } from '@nestjs/swagger';

export class GetExchangeDTO {
  @ApiProperty({
    default: 1.0,
  })
  amount: number;

  @ApiProperty({
    default: 'pen',
  })
  currency_ori: string;

  @ApiProperty({
    default: 'usd',
  })
  currency_des: string;
}
