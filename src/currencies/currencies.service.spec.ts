import { Test, TestingModule } from '@nestjs/testing';
import { CurrenciesService } from './currencies.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Currency } from './entities/currency.entity';
import { AuthGuard } from '@nestjs/passport';
import { CanActivate } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';

describe('CurrenciesService', () => {
  let service: CurrenciesService;
  let mockRepository;
  let mockHttp;
  const mockGuard: CanActivate = { canActivate: jest.fn(() => true) };

  beforeEach(async () => {
    mockRepository = {
      find: jest.fn(),
      findOne: jest.fn(),
      save: jest.fn(),
    };

    mockHttp = {
      axiosRef: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CurrenciesService,
        {
          provide: getRepositoryToken(Currency),
          useValue: mockRepository,
        },
        {
          provide: HttpService,
          useValue: mockHttp,
        },
        { provide: AuthGuard, useValue: mockGuard },
      ],
    }).compile();

    service = module.get<CurrenciesService>(CurrenciesService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
