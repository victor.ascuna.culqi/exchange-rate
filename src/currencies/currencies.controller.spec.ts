import { Test, TestingModule } from '@nestjs/testing';
import { CurrenciesController } from './currencies.controller';
import { CurrenciesService } from './currencies.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Currency } from './entities/currency.entity';
import { AuthGuard } from '@nestjs/passport';
import { CanActivate } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';

describe('CurrenciesController', () => {
  let controller: CurrenciesController;
  let mockRepository;
  let mockHttp;
  const mockGuard: CanActivate = { canActivate: jest.fn(() => true) };

  beforeEach(async () => {
    mockRepository = {
      find: jest.fn(),
      findOneBy: jest.fn(),
      create: jest.fn(),
      save: jest.fn(),
    };

    mockHttp = {
      axiosRef: {
        get: jest.fn(),
      },
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [CurrenciesController],
      providers: [
        CurrenciesService,
        {
          provide: getRepositoryToken(Currency),
          useValue: mockRepository,
        },
        {
          provide: HttpService,
          useValue: mockHttp,
        },
        { provide: AuthGuard, useValue: mockGuard },
      ],
    }).compile();

    controller = module.get<CurrenciesController>(CurrenciesController);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('get', () => {
    it('should return list of Currencies', async () => {
      const mockCurrencies = [
        { name: 'PEN' },
        { name: 'USD' },
        { name: 'EUR' },
        { name: 'CNY' },
      ];
      await mockRepository.find.mockResolvedValue(mockCurrencies);

      const result = await controller.findAll();
      expect(result).toStrictEqual(['PEN', 'USD', 'EUR', 'CNY']);
    });
  });

  describe('exchange', () => {
    it('should return Exchanged values', async () => {
      const mockCurrency = { value: 1.0 };
      await mockRepository.findOneBy.mockResolvedValue(mockCurrency);

      const request = {
        amount: 5,
        currency_ori: 'usd',
        currency_des: 'pen',
      };
      const result = await controller.getExchange(request);
      expect(result).toStrictEqual({
        amount: 5,
        currency_ori: 'usd',
        currency_des: 'pen',
        amount_exchanged: 5.0,
        exchange_rate: 1.0,
      });
    });
  });

  describe('add', () => {
    it('should return Currency', async () => {
      const mockAxios = {
        data: {
          rates: {
            PEN: 1.0,
          },
        },
      };
      const mockCurrency = {
        name: 'PEN',
        value: 1.0,
        id: 1,
      };
      await mockHttp.axiosRef.get.mockResolvedValue(mockAxios);
      await mockRepository.create.mockResolvedValue(mockCurrency);
      await mockRepository.save.mockResolvedValue(mockCurrency);

      const request = {
        name: 'pen',
      };
      const result = await controller.create(request);
      expect(result).toStrictEqual({
        name: 'PEN',
        value: 1.0,
        id: 1,
      });
    });
  });
});
