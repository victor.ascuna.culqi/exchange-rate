import { Controller, Get, Post, Body, UseGuards, Logger } from '@nestjs/common';
import { CurrenciesService } from './currencies.service';
import { CreateCurrencyDTO } from './dto/create-currency.dto';
import { GetExchangeDTO } from './dto/get-exchange.dto';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Cron } from '@nestjs/schedule';
import { Currency } from './entities/currency.entity';
import { ResponseExchangeDTO } from './dto/response-exchange.dto';

@ApiBearerAuth()
@ApiTags('currencies')
@UseGuards(JwtAuthGuard)
@Controller('currencies')
export class CurrenciesController {
  private readonly logger = new Logger(CurrenciesController.name);

  constructor(private readonly currenciesService: CurrenciesService) {}

  // check if exchange rates are out of date
  @Cron('0 0 * * * *')
  async handleCron() {
    const isUpdate = await this.currenciesService.update();
    if (isUpdate) {
      this.logger.log(`Update currencies`);
    } else {
      this.logger.log(`Validate currencies`);
    }
  }

  // get the currencies
  @Get()
  @ApiResponse({ status: 200, description: 'Ok.', type: String, isArray: true })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  findAll() {
    return this.currenciesService.findAll();
  }

  // add new currency
  @Post('add')
  @ApiResponse({ status: 200, description: 'Ok.', type: Currency })
  @ApiResponse({ status: 400, description: 'El tipo de moneda no existe.' })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({ status: 409, description: 'Ya se añadio el tipo de moneda.' })
  create(@Body() currency: CreateCurrencyDTO) {
    return this.currenciesService.create(currency);
  }

  // get currency exchanged
  @Post('exchange')
  @ApiResponse({ status: 200, description: 'Ok.', type: ResponseExchangeDTO })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({
    status: 404,
    description: 'No se encontro el tipo de moneda.',
  })
  getExchange(@Body() exchange: GetExchangeDTO) {
    return this.currenciesService.getExchange(exchange);
  }
}
