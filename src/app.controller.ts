import { Controller, Get, Logger } from '@nestjs/common';
import { AppService } from './app.service';
import { Cron } from '@nestjs/schedule';
import { HealthCheckService, TypeOrmHealthIndicator } from '@nestjs/terminus';
import { ApiResponse } from '@nestjs/swagger';

@Controller()
export class AppController {
  private readonly logger = new Logger(AppController.name);

  constructor(
    private readonly appService: AppService,
    private health: HealthCheckService,
    private db: TypeOrmHealthIndicator,
  ) {}

  @Get()
  @ApiResponse({ status: 200, description: 'Exchange Rate' })
  getHello(): string {
    return this.appService.getHello();
  }

  @Cron('*/5 * * * * *')
  async healthcheck() {
    const healthcheck = await this.health.check([
      () => this.db.pingCheck('exchangeRateDB'),
    ]);
    this.logger.log(`Conection with MySQL is ${healthcheck.status}`);
  }
}
