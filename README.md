# Description

Para el uso de los enpoints revisar Documentación en http://54.87.114.202:3000/documentation#/ .
La aplicación se encuentra desplegada en el siguiente IP y Puerto: 54.87.114.202:3000 

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Variables de Entorno
Existe un conjunto de variables de entorno configuradas en este proyecto que debera definir antes de levantar el proyecto
```bash
JWT_SECRET=AnyString
EXCHANGE_RATE_API=http://api.exchangeratesapi.io/v1/latest?access_key=TOKEN # El token lo puede generar gratis registrandose en https://exchangeratesapi.io/
MYSQL_HOST=localhost # mysqldb in docker
MYSQL_USER=root
MYSQL_PWD=victor
MYSQL_DB=exchangeRateDB
```

## Docker
Puede levantar la aplicación desde docker usando los siguientes comandos de `docker-compose`.

```bash
# Build
$ docker-compose up -d --build

# Stop
$ docker-compose down 
```

## First User
El primer usuario debe agregarse manualmente a la base de datos. Para ello acceder al MySQL e insertar el usuario admin.

```sql
# Insert
$ INSERT INTO exchangeRateDB.user (name, password) VALUES ('admin', 'admin')
```

## Installation
Instalación de librerias

```bash
$ npm install
```

## Running the app
Ejecución de la aplicación en local

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev
```

## Test
Para ejecutar los Test debe usar los siguientes comandos

```bash
# unit tests
$ npm run test
```
