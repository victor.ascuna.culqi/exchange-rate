# Run from Node 18
FROM node:18

ENV NODE_VERSION 18.19.0

# Create app directory
WORKDIR /app

# Install app dependencies
COPY package*.json /app/
RUN npm ci --ignore-scripts

# Bundle app source
COPY . .

# Make port 3000 available to the world outside this container
EXPOSE 3000

ENTRYPOINT npm start